
package pokemon;


class AnimalPokemon {
    // iniclaizados en contructor  
   private double vida;
   private double vidaMaxima ;
   
   
   protected String nombre;
   protected double  ataque; 
   protected double  defensa;
  // atrubutios 
   
   


    public AnimalPokemon() {
      
        vidaMaxima =100;
        vida= vidaMaxima;
    }
   

  
  public double  ataqueContricante(){
      System.out.println( nombre  + ": atacando   ");
      return ataque;
  }
  
  public void  calacularNuevaVida(){ 
     // el mejor de los casos dond
      vida= vida + vidaMaxima/2;
 
  }
   
  public void  calacularNuevaVida(double porcentaje){ 
      //poliformismo 
     // calcule la no al maximo maximo  de acuerdo al parametro que legue  
      vida= vida*(1+ (porcentaje/100));
  }
  
  
  public void  defensa(){
      System.out.println( nombre  + ": defendiendose   ");
             
  }

    public double getVida() {
        return vida;
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    public double getAtaque() {
        return ataque;
    }

    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    public double getDefensa() {
        return defensa;
    }

    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   
  
  
  
}
